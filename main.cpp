#include <iostream>
#include <ctime>

using namespace std;

class CoffeeMachine {
public:
    bool Start(int LevelWater) {
        if(CheckWater(LevelWater)) {
            cout << "Please, wait about 1 minute, while drink will be ready." << endl;
            return true;
        }
        else {
            cout << "Sorry, low level of water! Add more water." << endl;
            return false;
        }
    }
    virtual void Temperature() const {
        cout << "Temperature of Your drink 100 degrees" << endl;
    }
private:
    bool CheckWater(int LevelWater) {
        if(LevelWater > 100) {
            return true;
        }
        else {
            return false;
        }
    }
};

class Tea : public CoffeeMachine {
public:
    bool Start(int LevelWater) {
        CoffeeMachine cm;
        if(cm.Start(LevelWater)) {
            cout << "Price of Your drink 1$" << endl;
            return true;
        }
        else {
            cout << "Technical error" << endl;
            return false;
        }
    }
    void Temperature() const {
        cout << "Temperature of Your drink 80 degrees" << endl;
    }
};

class Limonad : public CoffeeMachine {
public:
    bool Start(int LevelWater) {
        CoffeeMachine cm;
        if(cm.Start(LevelWater)) {
            cout << "Price of Your drink 1.5$" << endl;
            return true;
        }
        else {
            cout << "Technical error" << endl;
            return false;
        }
    }
    void Temperature() const {
        cout << "Temperature of Your drink 30 degrees" << endl;
    }
};

class Coffee : public CoffeeMachine {
public:
    bool Start(int LevelWater) {
        CoffeeMachine cm;
        if(cm.Start(LevelWater)) {
            cout << "Price of Your drink 2$" << endl;
            return true;
        }
        else {
            cout << "Technical error" << endl;
            return false;
        }
    }
    void Temperature() const {
        cout << "Temperature of Your drink 70 degrees" << endl;
    }
};

void Temperature(const CoffeeMachine &obj) {
    obj.Temperature();
}

int main() {
    srand(time(NULL));
    int level = rand() % 1000;
    cout << "Level of water: " << level << endl;
    int option = 0;
    Tea t;
    Coffee c;
	Limonad l;
    cout << "Enter 1 - for coffee, 2 - for tea , 3 - for Limonad: " << endl;
    cin >> option;
    if(option == 1) {
        if(c.Start(level)) {
            Temperature(c);
        }
    }
    else if(option == 2) {
        if(t.Start(level)) {
            Temperature(t);
        }
    }
	else if(option == 3) {
        if(t.Start(level)) {
            Temperature(l);
        }
    }
    return 0;
}
